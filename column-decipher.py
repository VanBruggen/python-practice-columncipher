# ================================================== 1. Kryptogram i klucz:
# --------------------------------------------------```````` 1.1. Wczytaj kryptogram i klucz:

cryptogram = input("Podaj kryptogram: ")
key = input("Podaj klucz: ").upper()

# --------------------------------------------------```````` 1.2. Określ długość kryptogramu i klucza:

cryptogramLength = len(cryptogram)
keyLength = len(key)

# ================================================== 2. Tablica deszyfrująca:
# --------------------------------------------------```````` 2.1. Pomocnicza funkcja do wyświetlania tablicy:

def printTable(table):
    for r in table:
        print(r)

# --------------------------------------------------```````` 2.2. Oblicz liczbę wierszy:

rows = cryptogramLength // keyLength

# --------------------------------------------------```````` 2.3. Utwórz i wypełnij tablicę deszyfrującą iksami:

decipherTab = []
for z in range(rows):
    decipherTab.append([])
    for t in range(keyLength):
        decipherTab[z].append('X')

print('\ndecipherTab(1):')
printTable(decipherTab)

# --------------------------------------------------```````` 2.4. Wypełnij tablicę deszyfrującą znakami kryptogramu:

for g in range(keyLength):
    for h in range(rows):
        decipherTab[h][g] = cryptogram[g*rows + h]

print('\ndecipherTab(2):')
printTable(decipherTab)

# ================================================== 3. Tablica kolejności kolumn:
# --------------------------------------------------```````` 3.1. Utwórz pustą tablicę o długości klucza (placeholder
#                                                                 na indeksy kolumn):

columnOrder = []
for i in range(keyLength):
    columnOrder.append(None)

print('\ncolumnOrder(1):')
print(columnOrder)

# --------------------------------------------------```````` 3.2. Utwórz tablicową reprezentację klucza:

keyAsList = list(key)
print('\nkeyAsList:')
print(keyAsList)

# --------------------------------------------------```````` 3.3 Wypełnij pustą tablicę indeksami kolumn, zgodnie
#                                                                z alfabetyczną kolejnością znaków.

for counter in range(keyLength):
    ind = keyAsList.index(min(keyAsList))
    columnOrder[ind] = counter + 1
    keyAsList[ind] = '_'

print('\ncolumnOrder(2):')
print(columnOrder)


# ================================================== 4. Wyświetl tekst jawny:
# --------------------------------------------------```````` 4.1. Utwórz tekst jawny:

plainText = ''

# --------------------------------------------------```````` 4.2. Wypełnij tekst jawny:
# --------------------------------------------------```````````````` 4.2.1. Główna metoda -- odczytanie każdego znaku
#                                                                           wiersza, ale nie po kolei, tylko zgodnie
#                                                                           z numerami w tablicy kolejności kolumn,
#                                                                           tzn. jako pierwszy z danego wiersza zostanie
#                                                                           przekazany znak z kolumny o numerze
#                                                                           widniejącym na początku tablicy kolejności,
#                                                                           np. dla tablicy "[3, 1, 4, 2, 5]" jako
#                                                                           pierwszy zostanie przekazany znak z kolumny
#                                                                           3.

for i in range(rows):
    for x in range(keyLength):
        plainText += decipherTab[i][columnOrder[x]-1]

# --------------------------------------------------```````````````` 4.2.2. Alternatywna metoda -- przestawienie kolumn
#                                                                           i odczyt znaków po kolei. Wymaga stworzenia
#                                                                           dodatkowej tablicy, której indeksy
#                                                                           odpowiadają indeksom tablicy deszyfrującej
#                                                                           i która zawiera poprawną pozycję każdej
#                                                                           kolumny, tzn. którą kolumną powinna być
#                                                                           w tablicy wynikowej (zrobione wyłącznie
#                                                                           w ramach przećwiczenia paru zaawansowanych
#                                                                           poleceń Pythona :p).

'''columnSortingOrder = []
for n in range(keyLength):
    for k in range(keyLength):
        if columnOrder[k] == n+1:
            columnSortingOrder.append(k+1)

print('\ncolumnSortingOrder:')
print(columnSortingOrder)

decipherTabSorted = decipherTab.copy()
for rowIndex in range(rows):
    decipherTabSorted[rowIndex] = list(zip(decipherTabSorted[rowIndex], columnSortingOrder))
    decipherTabSorted[rowIndex].sort(key = lambda tup: tup[1])
    for m in decipherTabSorted[rowIndex]:
        plainText += m[0]

print('\ndecipherTabSorted:')
printTable(decipherTabSorted)'''

# --------------------------------------------------```````` 4.3. Wyświetl kryptogram i polecenie jego użycia jako
#                                                                 wiadomości w kolejnym wywołaniu programu, w celu
#                                                                 sprawdzenia poprawności deszyfracji.

print('\n' + plainText)