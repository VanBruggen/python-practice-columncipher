# ================================================== 1. Wiadomość i klucz:
# --------------------------------------------------```````` 1.1. Wczytaj wiadomość i klucz:

msg = input("Podaj wiadomosc: ")
key = input("Podaj klucz: ").upper()

# --------------------------------------------------```````` 1.2. Określ długość wiadomości i klucza:

msgLength = len(msg)
keyLength = len(key)

# ================================================== 2. Tablica szyfrująca:
# --------------------------------------------------```````` 2.1. Pomocnicza funkcja do wyświetlania tablicy:

def printTable(table):
    for r in table:
        print(r)

# --------------------------------------------------```````` 2.2. Oblicz liczbę wierszy:

rows = (msgLength // keyLength) if (msgLength % keyLength == 0) else ((msgLength // keyLength) + 1)

# --------------------------------------------------```````` 2.3. Utwórz i wypełnij tablicę szyfrującą iksami:

cipherTab = []
for z in range(rows):
    cipherTab.append([])
    for t in range(keyLength):
        cipherTab[z].append('X')

print('\ncipherTab(1):')
printTable(cipherTab)

# --------------------------------------------------```````` 2.4. Wypełnij tablicę szyfrującą znakami wiadomości:

for g in range(rows):
    for h in range(keyLength):
        cipherTab[g][h] = msg[g*keyLength + h]
        if (g*keyLength + h+1) == msgLength:
            break

print('\ncipherTab(2):')
printTable(cipherTab)

# ================================================== 3. Tablica kolejności kolumn:
# --------------------------------------------------```````` 3.1. Utwórz pustą tablicę o długości klucza (placeholder
#                                                                 na indeksy kolumn:

columnOrder = []
for i in range(keyLength):
    columnOrder.append(None)

print('\ncolumnOrder(1):')
print(columnOrder)

# --------------------------------------------------```````` 3.2. Utwórz tablicową reprezentację klucza:

keyAsList = list(key)
print('\nkeyAsList:')
print(keyAsList)

# --------------------------------------------------```````` 3.3 Wypełnij pustą tablicę indeksami kolumn, zgodnie
#                                                                z alfabetyczną kolejnością znaków.

for counter in range(keyLength):
    ind = keyAsList.index(min(keyAsList))
    columnOrder[ind] = counter + 1
    keyAsList[ind] = '_'

print('\ncolumnOrder(2):')
print(columnOrder)

# ================================================== 4. Wyświetl kryptogram:
# --------------------------------------------------```````` 4.1. Utwórz kryptogram:

cipherText = ''

# --------------------------------------------------```````` 4.2. Wypełnij kryptogram zgodnie z kolejnością kolumn:

for i in range(keyLength):
    for x in columnOrder:
        if x == i+1:
            columnIndex = columnOrder.index(x)
            for y in range(rows):
                cipherText += cipherTab[y][columnIndex]

# --------------------------------------------------```````` 4.3. Wyświetl kryptogram:

print('\n' + cipherText)