# Python practice -- Column cipher

Language: Polish.

A Python command line app, written for practice as part of _Data Protection Basics_ subject, during my CS studies.

A column encryptor and decyptor. A dissection of execution, along with relevant code parts, can be found in the attached PDF file (unfortunately only in Polish).

## Screenshots

![](./screenshots/column-cipher.png)

![](./screenshots/column-decipher.png)